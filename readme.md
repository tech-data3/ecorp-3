# ecorp-3

ETL project from simplon tech data formation.

## Links
### notion page of the project

https://apricot-bat-0d7.notion.site/ECORP-3-8003e83673d84aceacc88915353e457f

### git repo

https://gitlab.com/Nasyourte/ecorp-3

### git clone url

https://gitlab.com/Nasyourte/ecorp-3.git

## what we do

### goal

extract data from a file system and text file and load them in a database

### personal goal

follow OOP, TDD paradigm

use of pydantic, mysqlalchemy