import unittest
import src.Parser


class TestParser(unittest.TestCase):
    parser = src.Parser
    line_without_space = 'key:value'
    line_with_space_before_key_and_value = ' key: value'
    line_with_space_after_key_and_value = 'key :value '
    line_without_key = ': value'
    line_without_value = 'key :'
    line_with_not_a_valid_key = '$key:value'
    line_with_key_but_no_separator = 'key value'
    product_line = '74 Leather Hand Bag 57€ x 3'
    product_line_without_id = 'Leather Hand Bag 57€ x 3'

    def test_match_word(self):
        expected = 'key'
        result = src.Parser.match_word(self.line_without_space)
        self.assertEqual(expected, result.group(1))

    def test_match_word_with_space_before(self):
        expected = 'key'
        result = src.Parser.match_word(self.line_with_space_before_key_and_value)
        self.assertEqual(expected, result.group(1))

    def test_match_word_with_space_after(self):
        expected = 'key'
        result = src.Parser.match_word(self.line_with_space_after_key_and_value)
        self.assertEqual(expected, result.group(1))

    def test_match_word_invalid_key(self):
        expected = None
        result = src.Parser.match_word(self.line_with_not_a_valid_key)
        self.assertEqual(expected, result)

    def test_match_word_without_key(self):
        expected = None
        result = src.Parser.match_word(self.line_without_key)
        self.assertEqual(expected, result)

    def test_parse_line(self):
        expected = {'key': 'value'}
        self.parser.parse_line(self.line_without_space)
        self.assertEqual(expected['key'], self.parser.data['key'])

    def test_parse_line_with_space_before_key_value(self):
        expected = {'key': 'value'}
        self.parser.parse_line(self.line_with_space_before_key_and_value)
        self.assertEqual(expected['key'], self.parser.data['key'])

    def test_parse_line_with_space_after_key_value(self):
        expected = {'key': 'value'}
        self.parser.parse_line(self.line_with_space_after_key_and_value)
        self.assertEqual(expected['key'], self.parser.data['key'])

    def test_parse_line_without_value(self):
        expected = {'key': []}
        self.parser.parse_line(self.line_without_value)
        self.assertEqual(expected['key'], self.parser.data['key'])

    def test_parse_line_with_key_value_but_no_separator(self):
        with self.assertRaises(ValueError):
            self.parser.parse_line(self.line_with_key_but_no_separator)

    def test_parse_product(self):
        expected = {
            'id': 74,
            'name': 'Leather Hand Bag',
            'price': 57,
            'quantity': 3
        }
        result = self.parser.parse_product(self.product_line)
        self.assertEqual(expected, result)

    def test_parse_invalid_product(self):
        with self.assertRaises(ValueError):
            self.parser.parse_product(self.product_line_without_id)
