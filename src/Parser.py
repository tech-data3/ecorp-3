import re


def match_word(line: str):
    """
    match a word
    :param line: line str
    :return: Match
    """
    word = re.match(r'\s*([a-zA-Z#]+)', line)
    return word


def parse_product(line):
    """
    parse a product return a dictonary

    :param line: line str
    :return: dict
    """
    keys = ('id', 'name', 'price', 'quantity')
    types = (int, str, int, int)
    result = re.match(r'(\d+)\s(.*?)\s(\d+)€ x (\d+)', line)
    return dict(zip(keys, map(lambda f, v: f(v), types, result.groups())))


class Parser(object):
    """
    parser object, represent a parser take a path and give a dictonary
    """
    def __init__(self, path: str):
        self.path = path
        self.data = {}

    def parse_line(self, line: str):
        """
        parse a line

        :param line: str
        :return: void
        """
        key = match_word(line)
        if key is not None:
            sep = re.search(r'\s*:', line[key.end(0):])
            if sep is not None:
                value = match_word(line[key.end(0) + sep.end(0):])
                if value is not None:
                    self.data[key.group(1)] = value.group(1)
                else:
                    self.data[key.group(1)] = []
            else:
                raise ValueError('line has key but no separator')

    def parse(self):
        """
        parse an entire file
        :return:
        """
        pass
